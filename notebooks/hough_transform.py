#!/usr/bin/env python
# coding: utf-8

# In[1]:


import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

from osgeo import gdal_array


# In[2]:


path = '../data/geo/'
arr = gdal_array.LoadFile(path + 'lrr_4.tif')
h,w = arr.shape
ratio = w/h
iw = 14
ih = iw/ratio
(ratio, iw, ih)


# In[3]:


img = cv.normalize(src=arr, dst=None, alpha=0, beta=255, norm_type=cv.NORM_MINMAX, dtype=cv.CV_8UC1)


# In[4]:


(img.min(), img.max(), type(img[1,1]))


# In[5]:


def show_image(arr, title='Title'):
    fig = plt.figure(figsize=(iw,ih))
    plt.title(title)
    plt.imshow(arr, cmap='gray')


# In[73]:


img2 = cv.imread('../data/geo/water_cycles.jpg', cv.IMREAD_GRAYSCALE)


# In[74]:


show_image(img2, 'Original - Grayscale')


# In[7]:


img_noise = cv.medianBlur(img, 5)
show_image(img_noise, 'Original - Grayscale - Noise')


# In[75]:


img_canny = cv.Canny(img2, 40, 80)

show_image(img_canny, 'Canny')


# In[68]:


def calc_prob(arr):
    cnt = len(arr);
    col = np.empty((cnt, 1), np.float64)

    #print(col.shape)
    #print(arr.shape)

    arr = np.hstack((arr, col))

    step = 1/cnt;
    for i in range(cnt):
        #arr = np.float64(np.around(arr))     # such format changes array datatype
        arr[i][0] = round(arr[i][0])
        arr[i][1] = round(arr[i][1])
        arr[i][2] = round(arr[i][2])

        arr[i][3] = round(1 - i*step, 6)

    return arr

calc_prob([[1.4, 2.7, 3.2], [5.6, 7.7, 8.1], [1.1, 2.2, 3.3]])


# In[69]:


rows = h
acc_img_inv_ratio=1.7
canny_high_threshold=80
acc_threshold=4
max_radius = 100

circles = np.empty((0, 4), np.float64)

for i in range(max_radius):
    radius = i+1
    #print(radius)
    res = cv.HoughCircles(image=img_noise, method=cv.HOUGH_GRADIENT, dp=acc_img_inv_ratio, minDist=1,
                          param1=canny_high_threshold, param2=acc_threshold,
                          minRadius=radius, maxRadius=radius)
    res = calc_prob(res[0])
    circles = np.vstack((circles, res))


# In[70]:


circles.shape


# In[71]:


(circles[0:3], circles[2371953:2371955])
#(min(circles[:,2]), max(circles[:,2]))


# In[72]:


if circles is not None:
    #circles = np.uint16(np.around(circles))
    np.savetxt(path + 'hough_circles_2.csv', circles, delimiter=',', fmt='%d,%d,%d,%.6f', header='x,y,z,prob')


# In[104]:


tgt = np.copy(img)

if circles is not None:
    circles = np.uint16(np.around(circles))
    for i in circles[:]:
        if i[2] > 10:
            center = (i[0], i[1])
            # circle center
            cv.circle(tgt, center, 1, (0, 100, 100))
            # circle outline
            radius = i[2]
            cv.circle(tgt, center, radius, (255, 0, 255))


# In[105]:


show_image(tgt, 'Radius - ' + str(radius))


# In[121]:


circles.shape

