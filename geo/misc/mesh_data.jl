# 3D mesh
function mesh_data(x, y, z)
    # This is basically a structured grid, but defined as an unstructured one.
    # Based on the structured.jl example.
    Ni, Nj, Nk = x, y, z
    indices = LinearIndices((1:Ni, 1:Nj, 1:Nk))
    Npts = length(indices)

    # Create points and point data.
    pts_ijk = Array{FloatType}(undef, 3, Ni, Nj, Nk)
    pdata_ijk = Array{FloatType}(undef, Ni, Nj, Nk)

    for k = 1:Nk, j = 1:Nj, i = 1:Ni
        r = 1 + (i - 1)/(Ni - 1)
        th = 3*pi/4 * (j - 1)/(Nj - 1)
        pts_ijk[1, i, j, k] = r * cos(th)
        pts_ijk[2, i, j, k] = r * sin(th)
        pts_ijk[3, i, j, k] = (k - 1)/(Nk - 1)
        pdata_ijk[i, j, k] = i*i + k*sqrt(j)
    end

    # Create cells (all hexahedrons in this case) and cell data.
    celltype = VTKCellTypes.VTK_POLYGON
    cells = MeshCell[]
    cdata = FloatType[]

    for k = 2:Nk, j = 2:Nj, i = 2:Ni
        # Define connectivity of cell.
        inds = Array{Int32}(undef, 8)
        inds[1] = indices[i-1, j-1, k-1]
        inds[2] = indices[i  , j-1, k-1]
        inds[3] = indices[i  , j  , k-1]
        inds[4] = indices[i-1, j  , k-1]
        inds[5] = indices[i-1, j-1, k  ]
        inds[6] = indices[i  , j-1, k  ]
        inds[7] = indices[i  , j  , k  ]
        inds[8] = indices[i-1, j  , k  ]

        # Define cell.
        c = MeshCell(celltype, inds)

        push!(cells, c)
        push!(cdata, i*j*k)
    end

    pts = reshape(pts_ijk, 3, Npts)
    pdata = reshape(pdata_ijk, Npts)

    return pts, cells, pdata, cdata
end