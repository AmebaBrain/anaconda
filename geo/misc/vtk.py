import os
import numpy as np

from evtk.hl import pointsToVTK

directory = '/home/toleg/vanya/repo/data/geo'
MAX_COUNT = 300
counter = 0
data = []
arr = []

for filename in os.listdir(directory):
    if counter <= MAX_COUNT:
        if filename.startswith("cube") and filename.endswith(".csv"):
            print('processing ' + filename)
            data = np.genfromtxt(os.path.join(directory, filename), skip_header=1, delimiter=',', dtype=np.int16)
            if counter == 0:
                arr = data
            else:
                arr = np.concatenate((arr, data))

            #print('shape: ' + str(data.shape))
            #print('acc shape: ' + str(arr.shape))
            counter += 1
        else:
            print('non-matching file ' + filename)

print('accumulated shape: ' + str(arr.shape) + ', layers: ' + str(counter))
x = np.ascontiguousarray(arr[:, 0])
y = np.ascontiguousarray(arr[:, 1])
z = np.ascontiguousarray(arr[:, 2])
val = np.ascontiguousarray(arr[:, 3])

"""
print(type(x).__name__ + str(x.shape) + ', flags: ' + str(x.flags))
print(type(y).__name__ + str(y.shape) + ', flags: ' + str(y.flags))
print(type(z).__name__ + str(z.shape) + ', flags: ' + str(z.flags))
print(type(val).__name__ + str(val.shape) + ', flags: ' + str(val.flags))
"""

pointsToVTK("./cube", x, y, z, data={"val": val})
