using Images, ImageFeatures, FileIO, ImageView
using WriteVTK
using ConfParser
using CSV
using DataFrames
using Statistics
using SharedArrays
using Distributed
using GeoCircle

@everywhere push!(LOAD_PATH, "/home/toleg/vanya/repo/geo")
@everywhere using SharedArrays, ImageFeatures, GeoCircle

function create_vti()
    conf = init("../GeoCircle/test/cube.ini")
    rmin = parse(Int, retrieve(conf, "calculation", "rmin"))
    rmax = parse(Int, retrieve(conf, "calculation", "rmax"))

    img = getdata(conf)
    #img = Gray.(img)
    #img = round.(255 * minimax(img))
    #img = mapwindow(median!, img, (5, 5))

    img_edges = canny(img, (Percentile(93), Percentile(50)))
    dx, dy = imgradients(img, KernelFactors.ando3)
    img_phase = phase(dx, dy)

    (row, col) = size(img)
    # this initialization from the docs doesn't work
    #cube = SharedArray{Float64}((row, col, rmax), init = S -> S[localindices(S)] = 0)
    cube = zeros(Float64, (col, row, rmax))
    cube = convert(SharedArray, cube)
    #df = DataFrame(x = Int[], y = Int[], z = Int[], prob = Float64[])

    @sync @distributed for r in rmin:rmax
        @time circles, voters, radii = hough_circle_gradient(img_edges, img_phase, r:r, min_dist=1, vote_threshold=4)
        @info "radius - $(r), circles - $(size(voters)[1])"

        #voters = minimax(voters)
        # stat normalization
        #m = mean(voters)
        #s = std(voters, corrected=false)
        #voters = (voters .- m) / s

        for i in 1:size(circles)[1]
            cube[circles[i][2], circles[i][1], radii[i]] = voters[i]
            #push!(df, (circles[i][2], circles[i][1], radii[i], voters[i]))
        end
    end

    writefile(conf, cube)

    cube
end
