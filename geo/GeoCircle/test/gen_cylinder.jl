using WriteVTK

function gen_cylinder(width::Int, height::Int, xc::Int, yc::Int, r::Int, vh::Int)
    img = zeros(Int, height, width, vh)

    for v in 1:vh
        #=
        Circumference for a circle of radius r should be 2*pi*r pixels.
        To ensure that we have no gaps in the circle
        we need to make sure we have at least as many coordinates in vectors x and y
        as there are around the circumference of the circle.
        Make it double that just to make extra sure there are no gaps in the circle
        by going all 360 degrees (0 - 2*pi)
        =#
        theta = LinRange(0, 2 * pi, round(Int, 4 * pi * r)) # Define angles

        for i in 1:r
            # Get x and y vectors for each point along the circumference.
            x = i .* cos.(theta) .+ xc
            y = i .* sin.(theta) .+ yc

            # Write those (x,y) into the image with gray level 255.
            for j = 1:length(x)
                row = round(Int, y[j])
                col = round(Int, x[j])
                if checkbounds(Bool, img, row, col, v)
                    @inbounds img[row, col, v] = round(255 * i / r)
                end
            end
        end
    end

    vtk_write_array("../../../data/geo/cylinder", img, "value")

    img
end