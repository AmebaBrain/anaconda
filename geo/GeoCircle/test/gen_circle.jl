using Images
using DataFrames

"""
Code to burn a white circle into a black digital image.
Write white pixels along the circumference of the circle into an image.
"""
function gen_circle(width::Int, height::Int, xc::Int, yc::Int, rmin::Int, rmax::Int = rmin)
    img = zeros(Int8, height, width)

    for radius in rmin:rmax
        #=
        Circumference for a circle of radius r should be 2*pi*r pixels.
        To ensure that we have no gaps in the circle
        we need to make sure we have at least as many coordinates in vectors x and y
        as there are around the circumference of the circle.
        Make it double that just to make extra sure there are no gaps in the circle
        by going all 360 degrees (0 - 2*pi)
        =#
        theta = LinRange(0, 2 * pi, round(Int, 4 * pi * radius)) # Define angles

        # Get x and y vectors for each point along the circumference.
        x = radius .* cos.(theta) .+ xc
        y = radius .* sin.(theta) .+ yc

        # Write those (x,y) into the image with gray level 255.
        for i = 1:length(x)
            row = round(Int, y[i])
            col = round(Int, x[i])
            if checkbounds(Bool, img, row, col)
                img[row, col] = 1
            end
        end
    end

    img
end

"""
Generate circumference mask with radius `r`
"""
function gen_mask(r::Int)::Matrix{Int8}
    gen_circle(2*r + 1, 2*r + 1, r + 1, r + 1, r, r)
end


function circle_width_prob(rmax::Int, wmax::Int)

    df = DataFrame(radius = Int16[], width = Int16[], prob = Float64[])

    for r in 1:rmax
        # create image with current radius and maximum width
        side = 2*(r + wmax - 1) + 1
        img = round.(Int, rand(side, side))

        for w in 0:wmax-1
            # create mask with current radius and current width
            mask = centered(gen_circle(2*r + 1, 2*r + 1, r + 1, r + 1, r, r + w))

            # filter image with mask and define prob for the central pixel only
            imgf = imfilter(Float64, img, mask, Fill(0, mask))
            prob = imgf[r, r] / sum(mask)

            push!(df, Dict(:radius => r, :width => w + 1, :prob => prob))
        end
    end

    df
end