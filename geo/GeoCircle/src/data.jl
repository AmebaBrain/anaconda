using ConfParser
using CSV
using FileIO
using Images
using WriteVTK

using ArchGDAL
const AG = ArchGDAL

"""
Define file type by it's extension
"""
_filetype(filename::String)::Symbol = Symbol(SubString(filename, findlast(".", filename).start + 1))

"""
Get file name without extension
"""
_filename(filename::String)::String = SubString(filename, 1, findlast(".", filename).start - 1)

"""
Geo coordinates
- `x`, `y` - Coordinates of image origin. Default is upper left corner for source *.tif files.
             For result Paraview cube it is bottom left corner which corresponds to Cartesian coordinate system
- `xstep`, `ystep` - pixel increment value in X and Y directions respectively
"""
mutable struct Coordinates
    x::Float64
    y::Float64
    xstep::Float64
    ystep::Float64
end

"""
    function readtif(filename::Union{String, Array{String,1}})::Tuple{AbstractArray, Coordinates}

Read *.tif file and return its data and geo coordinates
"""
function readtif(filename::Union{String, Array{String,1}})::Tuple{AbstractArray, Coordinates}

    if typeof(filename) === String
        files = Array{String,1}(undef, 1)
        files[1] = filename
    else
        files = filename
    end

    local arr
    local coords

    for i in axes(files, 1)
        AG.read(files[i]) do dataset
            @info "file: $(AG.filelist(dataset)), rasters/bands: $(AG.nraster(dataset))"

            # we are interested in first band only
            band = AG.getband(dataset, 1)

            data = AG.read(band)
            # returned shape is (col, row). we have to transpose it
            # data = data' # old implementation
            data = PermutedDimsArray(data, (2, 1));

            if i == 1
                T = AG.pixeltype(band)
                y, x = size(data)

                arr = Array{T, 3}(undef, y, x, length(files))
                @info typeof(arr), size(arr)

                gt = AG.getgeotransform(dataset)
                coords = Coordinates(gt[1], gt[4], gt[2], gt[6])
            end

            @info """band: $(AG.indexof(band)), width: $(AG.width(band)), height: $(AG.height(band)),
                        scale: $(AG.getscale(band)), offset: $(AG.getoffset(band)), unittype: $(AG.getunittype(band)),
                        coordinates: $coords
                  """

            arr[:,:,i] = data

            #=
            @debug repr(dataset)
            @debug "Number of rasters: $(AG.nraster(dataset))"
            @debug "Data Size: $(repr(size(data)))"
            @debug "Number of unique values: $(convert.(Int, unique(data)))"

            =#
        end
    end

    arr, coords
end

"""
    function setcoords!(c::Coordinates, dims::NTuple{N, Int}, xs::Int = 0, ye::Int = 0) where N

In order to link cube in Paraview we need to specify `origin` and `spacing` parameters to WriteVTK API.
Coordinate increment defined by `spacing` must be positive in order to have Volume Rendering mode in Paraview display anything.

Geo Coordinates `c` obtained from original _*.tif_ files could have negative increment.
This is a sign that we have maximum coordinate specified. In order to get initial coordinate we have to calculate it
by substracting corresponding image dimensions total pixels size.
"""
function setcoords!(c::Coordinates, dims::NTuple{N, Int}, xs::Int = 0, ye::Int = 0) where N

    h, w = dims[1], dims[2]

    # if increment by some coordinate is negative this means that we have maximum coordinate specified
    # in order to get initial coordinate we have to calculate it by substracting corresponding image dimensions total pixels size
    if c.xstep < 0
        c.xstep = abs(c.xstep)
        c.x = c.x - w * c.xstep
    end
    if c.ystep < 0
        c.ystep = abs(c.ystep)
        c.y = c.y - h * c.ystep
    end

    # in partial mode we have `xs` defined in terms of image top left corner pixels coordinates with start at 1
    # for `x` axis direction in top left coordinates is the same as in cartesian coordinates
    if xs != 0
        c.x = c.x + (xs - 1) * c.xstep
    end

    # in partial mode we have `ye` defined in terms of image top left corner pixels coordinates with start at 1
    # for `y` axis direction in top left coordinates is opposite to cartesian one
    if ye != 0
        c.y = c.y + (h - ye + 1) * c.ystep
    end

    @info "Adjusted coordinates: $c"
end

function readcsv(filename::String)::Array{T,2} where T
    convert(Array{T,2}, CSV.read(filename))
end

function getdata()::Tuple{AbstractArray, Coordinates}
    path = retrieve(conf, "environment", "path")
    file = retrieve(conf, "environment", "file")

    # file=filename.ext                                 - single file
    # file=filename1.ext,filename2.ext,...filenameN.ext - list (1D array)
    # many files will be processed simultaneously for each radius
    # circles will be searched for each input file and summed up for each radius
    if typeof(file) !== Array{String,1}
        filepath = path * file
        filetype = _filetype(filepath)
    else
        filepath = path .* file
        filetype = _filetype(filepath[1])
    end

    if filetype === :tif
        data, coords = readtif(filepath)
    elseif filetype === :csv
        data = readcsv(filepath)
        coords = Coordinates(0, 0, 0, 0)
    elseif filetype === :png
        data = Gray.(load(filepath))
        coords = Coordinates(0, 0, 0, 0)
    end

    ispart = parse(Bool, retrieve(conf, "calculation", "ispart"))
    if ispart
        ys = parse(Int, retrieve(conf, "calculation", "ys"))
        ye = parse(Int, retrieve(conf, "calculation", "ye"))
        xs = parse(Int, retrieve(conf, "calculation", "xs"))
        xe = parse(Int, retrieve(conf, "calculation", "xe"))

        ys = min(ys, size(data)[1])
        ye = min(ye, size(data)[1])
        xs = min(xs, size(data)[2])
        xe = min(xe, size(data)[2])

        @assert ys < ye "Y start position $ys is greater or equal than Y end postion $ye"
        @assert xs < xe "X start position $xs is greater or equal than X end postion $xe"

        @info "Partial mode. Selected area size: ($ys:$ye, $xs:$xe)"
        if length(size(data)) == 2
            res = data[ys:ye, xs:xe]
        else
            res = data[ys:ye, xs:xe, :]
        end

        setcoords!(coords, size(data), xs, ye)
    else
        res = data
        setcoords!(coords, size(data))
    end

    res, coords
end

"""
Generate output file name
"""
function genname(T::DataType)::String
    file = retrieve(conf, "paraview", "file")

    if file == "{env_file}"
        file = retrieve(conf, "environment", "file")
        if typeof(file) === Array{String,1}
            file = file[1]
        end
        file = _filename(file)
    end

    istest = parse(Bool, retrieve(conf, "calculation", "istest"))
    ispart = parse(Bool, retrieve(conf, "calculation", "ispart"))
    if istest
        file = "test_" * file
    elseif ispart
        file = "partial_" * file
    end

    path = retrieve(conf, "environment", "path")
    path = path * file

    radius = retrieve(conf, "wave", "radius")
    scale = retrieve(conf, "wave", "scale_factor")
    rmin = retrieve(conf, "calculation", "rmin")
    rmax = retrieve(conf, "calculation", "rmax")
    threshold = retrieve(conf, "hough_circles", "acc_threshold")

    suffix = "_wave" * radius *  "_scale" * scale * "_threshold" * threshold * "_r" * rmin * "_r" * rmax
    suffix = suffix * "_" * lowercase(string(T))

    path * suffix
end

"""
    function writefile(cube::AbstractArray{T}) where T

Write calculated cube to *.vti file and dump array into *.jld2 file.

#Arguments
- `cube::AbstractArray{T}`: calculated superposition of waves 3D fronts
- `coords::Coordinates`: geo coordinates of original *.tif file
"""
function writefile(cube::AbstractArray{T}, c::Coordinates = Coordinates(0, 0, 1, 1)) where T
    file = genname(eltype(cube))

    # WriteVTK library consider array dimensions as (x, y, z)
    # thus we have to swap Y and X dimensions via PermutedDimsArray to leverage system coordinate orientation in WriteVTK
    cube = PermutedDimsArray(cube, (2, 1, 3))

    # in order to depict higher radiuses as depth with negative values we have to _reverse_ z axis
    cube = @view cube[:,:,end:-1:1]

    # approximation
    zstep = (c.xstep + c.ystep) / 2
    zstart = zstep*(1 - size(cube)[3])

    @info "coordinates: $c, zstep: $zstep, zstart: $zstart"

    # writing *.vti file
    vtk = vtk_grid(file, size(cube), origin=[c.x, c.y, zstart], spacing = [c.xstep, c.ystep, zstep]);
    vtk["energy"] = cube;
    vtk_save(vtk)

    # dumping calculated waves
    save(file * ".jld2", "cube", cube)
end