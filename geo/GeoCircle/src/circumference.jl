"""
Create circumference of the circle with a given radius `r` on a square image of size `2*r + 1`
Returns 2d array which consists of white (value=1) pixels on a black (pixel=0) background.
"""
function circumference(r::Integer)
    # center coordinates
    xc = r + 1;
    yc = r + 1;

    #=
    Circumference for a circle of radius r is 2*pi*r.
    To ensure that we have no gaps in the circle we need to make sure we have at least as many coordinates in vectors x and y
    as there are around the circumference of the circle.
    Make it double that just to make extra sure there are no gaps in the circle by going all 360 degrees (0 - 2*pi).
    =#
    theta = LinRange(0, 2*pi, round(Int, 4 * pi * r)) # Define angles

    # Get x and y vectors for each point along the circumference.
    x = r .* cos.(theta) .+ xc
    y = r .* sin.(theta) .+ yc

    # result array size is defined by input radius
    a = 2*r + 1
    arr = zeros(Int8, a, a)

    # Write those (x,y) into the image with gray level 255.
    for i = 1:length(x)
        row = round(Int, y[i])
        col = round(Int, x[i])
        arr[row, col] = 1
    end

    arr
end