using DataFrames
using ImageView

"""
Given radius `r` and gradient angle `theta` (biggest between y and x directions) returns circle centers as `CartesianIndex`
"""
function ccenter(r::Int, img::Matrix, theta::Matrix{Float64})::Matrix{CartesianIndex{2}}
    cc = fill(CartesianIndex(0,0), size(theta))

    for index in findall(x -> x > 0, img)
        yc = round(Int, index[1] - r * sin(theta[index]))
        xc = round(Int, index[2] - r * cos(theta[index]))
        @info "$(index) - $(yc) - $(xc)"
        cc[index] = CartesianIndex(yc, xc)
    end

    return cc
end

"""
Applies minimax transform for given array
"""
function minimax(arr::AbstractArray)::AbstractArray{Float64}
    arr = (arr .- minimum(arr))/(maximum(arr) - minimum(arr))
    convert.(Float64, arr)
end

"""
Hough Gradient Circles implementation
"""
function hough_circles(img::Matrix, rmin::Int = 1, rmax::Int = 100, localmax::Bool = false)::Tuple{Array{CalcType, 3}, Array{Int32, 3}}
    acc_threshold = parse(Int, retrieve(conf, "hough_circles", "acc_threshold"))
    grad_threshold = parse(Float64, retrieve(conf, "hough_circles", "grad_threshold"))
    row, col = size(img)
    z = rmax - rmin + 1

    acc = zeros(Int32, row, col, z)
    cube = zeros(CalcType, row, col, z)

    # we need "orientation" method output inside "imedge" function. it calculates basic "atan(grad_y, grad_x)"
    # in contrast "phase" method calculates perpendiculaer angle which results into edges detection instead of centers
    _, _, mag, theta = imedge(img)

    # we could have result gradient magnitude larger than 1
    # but we have filtering by gradient threshold later on
    gmax = maximum(mag)
    mag = gmax > 0 ? mag / gmax : mag

    for r in 1:z
        for idx in findall(x -> x > grad_threshold, mag)
            yc = round(Int, idx[1] - (r + rmin - 1) * sin(theta[idx]))
            xc = round(Int, idx[2] - (r + rmin - 1) * cos(theta[idx]))

            # TODO: attempt to adjust single pixel shift to real edge because gradient makes a single pixel shift
            # gradient at actual edge pixel is 0
            #yc = round(Int, (sin(theta[idx]) > 0 ? -1 : 1) + idx[1] - (r + rmin - 1) * sin(theta[idx]))
            #xc = round(Int, (cos(theta[idx]) > 0 ? -1 : 1) + idx[2] - (r + rmin - 1) * cos(theta[idx]))

            #@info "$(idx) - $(yc) - $(xc)"
            if checkbounds(Bool, acc, yc, xc, r)
                @inbounds acc[yc, xc, r] += 1
            end
        end

        if localmax
            # findlocalmaxima is 2d function
            # it leads to excessive filtering which sweep out real centers
            for idx in findlocalmaxima(acc[:,:,r])
                if acc[idx, r] >= acc_threshold
                    cube[idx, r] = acc[idx, r]
                end
            end
        else
            # alternative approach that don't use findlocalmaxima
            # will bring more "duplicated" data smashed around each circle centers
            for idx in findall(x -> x >= acc_threshold, acc[:,:,r])
                cube[idx, r] = acc[idx, r]
            end
        end

        # !!! cube should be normalized over max value per calculation radius layer !!!
        # because it's naturally that for bigger radiuses we will have higher number of matched points
        # but for example N matched points for radius = 5 weight _more_ than for radius = 10
        cmax = maximum(cube[:,:,r])
        if cmax > 0
            cube[:,:,r] = cube[:,:,r] / cmax
        end
    end

    cube, acc
end

function print_circles(img::Matrix, centers::Array{CalcType, 3}, topn::Int = 10; radius::Int = 0)
    if typeof(img) !== Array{RGB{Normed{UInt8,8}},2}
        imgc = RGB.(img, img, img)
    else
        imgc = RGB.(copy(img))
    end

    df = DataFrame(y = Int16[], x = Int16[], r = Int16[], w = CalcType[])

    for idx in findall(x -> x > 0, centers)
        if radius == 0 || size(centers, 3) == 1 || idx[3] == radius
            push!(df,(idx[1], idx[2], size(centers, 3) == 1 ? radius : idx[3], centers[idx]))
        end
    end

    sort!(df, :w, rev=true)

    df = first(df, topn)

    for c in eachrow(df)
        # define 3x3 red cross in circle center
        for i in -1:1, j in -1:1
            if abs(i + j) == 1 || (i == 0 && j == 0)
                if checkbounds(Bool, imgc, c.y + i, c.x + j)
                    imgc[c.y + i, c.x + j] = RGB(1, 0, 0)
                end
            end
        end

        # Define circumference with 2*2πr points to avoid wholes in edges
        θ = LinRange(0, 2π, round(Int, 4π*c.r))

        # Get x and y vectors for each point along the circumference.
        x = c.r .* cos.(θ) .+ c.x
        y = c.r .* sin.(θ) .+ c.y

        # Write those (x,y) into the image with gray level 255.
        for i = 1:length(x)
            row = round(Int, y[i])
            col = round(Int, x[i])
            if checkbounds(Bool, imgc, row, col)
                imgc[row, col] = RGB(0, 1, 0)
            end
        end
    end

    imgc
end
