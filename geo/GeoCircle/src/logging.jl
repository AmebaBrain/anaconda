using Logging
using ConfParser


function setloglevel(conf::ConfParse)
    conf_log_level = retrieve(conf, "environment", "log_level")

    if conf_log_level == "debug"
        log_level = Logging.Debug
    elseif conf_log_level == "info"
        log_level = Logging.Info
    elseif conf_log_level == "warn"
        log_level = Logging.Warn
    elseif conf_log_level == "error"
        log_level = Logging.Error
    else
        log_level = Logging.Info
    end
    logger = ConsoleLogger(stderr, log_level)
    global_logger(logger)
end

