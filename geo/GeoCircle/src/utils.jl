using Distributed

"""
Adding workers processes
"""
function addworkers(maxcap=false)
    jobs_cnt = parse(Int, retrieve(conf, "calculation", "workers"))

    if maxcap
       jobs_cnt = min(jobs_cnt, Sys.CPU_THREADS)
    end

    if nworkers() == 1
        addprocs(jobs_cnt)
    else
        addprocs(jobs_cnt - nworkers())
    end

    if jobs_cnt > 0
        @info "Parallel execution: $jobs_cnt workers"
    else
        @info "Serial execution"
    end

    @everywhere push!(LOAD_PATH, normpath(String(@__DIR__) * "/../.."))
    eval(macroexpand(Distributed, quote @everywhere using GeoCircle end))

    jobs_cnt
end

"""
Remove worker processes and  tmp memory mapped file
"""
function cleanup()
    if nworkers() > 1
        rmprocs(workers())
    end

    tmpfile = retrieve(conf, "calculation", "tmp_file_path")
    rm(tmpfile; force=true)
    GC.gc()
end