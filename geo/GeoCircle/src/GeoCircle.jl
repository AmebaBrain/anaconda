module GeoCircle

confpath = nothing
conf = nothing

const CalcType = Float32

ParaViewType = nothing

include("utils.jl")
include("logging.jl")
include("data.jl")
include("surfature.jl")
include("circumference.jl")
include("hough.jl")
include("wave.jl")
include("cube.jl")

function __init__()
    global confpath = get(ENV, "GEO_CIRCLE_INI", normpath(String(@__DIR__) * "/../test/cube.ini"))
    init(confpath)
end

export
    # utils.jl
    addworkers, cleanup,
    # data.jl
    readtif, readcsv, getdata, writefile,
    # surfature.jl
    gsurfature,
    # circumference.jl
    circumference,
    # hough.jl
    hough_circles, ccenter, minimax, print_circles,
    # wave.jl
    genwave, getdict, centers, waves3d,
    _fse, _fzse, _cse, _czse,
    # cube.jl
    init, main
end


