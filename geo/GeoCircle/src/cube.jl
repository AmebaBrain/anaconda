using ConfParser

"""
Reload configuration settings from `file`
"""
function init(file::String = confpath)
    @info "Parsing config file"
    c = ConfParse(file)
    parse_conf!(c)

    global conf = c
    #global CalcType = eval(Symbol(retrieve(c, "calculation", "datatype")))  # linter error. every module has an 1 argument eval method
    global ParaViewType = eval(Symbol(retrieve(c, "paraview", "datatype")))  # linter error. every module has an 1 argument eval method

    #setloglevel(conf)
end

"""
Main execution flow
"""
function main()::Array{ParaViewType, 3}
    init()

    @info "Load data"
    @time data, coords = getdata()

    rmin = parse(Int, retrieve(conf, "calculation", "rmin"))
    rmax = parse(Int, retrieve(conf, "calculation", "rmax"))
    wradius = parse(Int, retrieve(conf, "wave", "radius"))

    addworkers()

    @info "Build cube"
    waves = waves3d(data, rmin, rmax, wradius)

    @info "Write result"
    @time writefile(waves, coords)

    @info "Cleanup"
    cleanup()

    waves
end
