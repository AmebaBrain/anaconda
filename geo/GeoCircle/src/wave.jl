using OffsetArrays
using SharedArrays
using FileIO
using Distributions
using ConfParser
using DataStructures

function getdict()::DataStructures.SortedDict
    scale    = parse(Int, retrieve(conf, "wave", "scale_factor"))
    path     = retrieve(conf, "wave", "lib_path")
    filepath = path * retrieve(conf, "wave", "file") * "_scale_" * string(scale) * ".jld2"

    if isfile(filepath)
        @info "Found precalculated file $filepath"
        dict = load(filepath, "pdf")
    else
        distribution = retrieve(conf, "wave", "distribution")

        if Symbol(distribution) === :BetaPrime
            d = BetaPrime()
        elseif Symbol(distribution) === :Normal
            d = Normal()
        else
            d = nothing
        end

        _pdf(r::Float64) = r == 0 ? 1 : pdf(d, r)
        _dist(i::Int, j::Int) = i + j/10

        max_dist = parse(Int, retrieve(conf, "wave", "pdf_max_dist")) - 1

        @info "Creating dict of pdf values from 0 to $max_dist with 0.1 step"
        @time dict = Dict{Float64, Float64}(_dist(i,j) => _pdf(_dist(i,j)/scale) for i in 0:max_dist, j in 0:9)
        @info "Converting dict to SortedDict"
        @time dict = DataStructures.SortedDict(dict)

        @info "Final length: $(length(dict))"
        @info "Storing file $filepath"
        save(filepath, "pdf", dict)
    end

    dict
end

#=
"""
For every point calculate intensity as combination of intensity impulses from every circle center
Circle impulse direction is defined by vector from circle center to the given point
"""
function waves2d(conf::ConfParse, img::Matrix{Float64})::Matrix{Float64}

    # get hashmap with BetaPrime distribution pdf values
    dict = getdict(img)

    # result array
    res = similar(Matrix{Float64}, axes(img))

    # outer loop for every pixel
    for y in axes(res, 1), x in axes(res, 2)
        #@info "row: $y - col: $x"

        # y, x intensity projections for every circle centers
        proj = Matrix{Float64}(undef, length(img[img .> 0]), 2)
        # current pointer in "proj" array
        i = 0

        # inner loop by all circle centers
        for idx in findall(x -> x > 0, img)
            yc = idx[1]
            xc = idx[2]
            i += 1

            # coordinate system center is top left with Y axis going down and X axis right
            # we are calculating angle between Y axis and vector started from circle center (yc, xc) and ended at current (y,x) point
            # for circle center we equally split intensity among components to get module equal to original value
            if xc == x && yc == y
                proj[i,1] = img[idx] / sqrt(2)
                proj[i,2] = img[idx] / sqrt(2)
            else
                # intersection angle between radius vector and X axis
                α = atan((yc - y)/(xc - x))

                # distance from circle center
                len = sqrt((x - xc)^2 + (y - yc)^2)

                # we don't sum vectors here. instead we project intensity in a given point onto the axes
                # because "cos" is even function and "sin" is not we have to use absolute their values to act as a weight coefficients only
                # impulse direction is dictated by radius vector
                proj[i,1] = img[idx] * sign(xc - x) * abs(cos(α)) * dict[len]   # x projection
                proj[i,2] = img[idx] * sign(yc - y) * abs(sin(α)) * dict[len]   # y projection
            end
        end

        # summing up projected components per coordinate basis: sqrt(Y^2 + X^2)
        res[y,x] = sqrt(sum(proj[:,1])^2 + sum(proj[:,2])^2)

    end

    res
end
=#

"""
Generate 3d spherical decaying wave with radius `r`. Parameter `semi` defines wether to generate the whole or only half of a sphere
"""
function genwave(r::Int = 100; semi::Bool = true)::SharedArray{CalcType, 3}
    # get hashmap with BetaPrime distribution pdf values
    dict = getdict()

    # we generate spherical points via symmetrical OffsetArray arrays in range -r:r
    # in order to preserve actual radius length in generated wave we have to use r-1 in calculations
    #r = r - 1

    if semi
        # for semi wave impulse center have to be at the very bottom layer
        f = -r
        l = 0
    else
        f = -r
        l = r
    end

    wave = OffsetArray(zeros(CalcType, 2r+1, 2r+1, l-f+1), -r:r, -r:r, f:l)

    for i in -r:r, j in -r:r, k in f:l

        len = sqrt(i^2 + j^2 + k^2)
        if len <= r
            if checkbounds(Bool, wave, i, j, k)
                @inbounds wave[i, j, k] = dict[round(len, digits=1)]
            else
                error("Bounds check error: $i:$j:$k")
            end
        end
    end

    convert(SharedArray, parent(wave))
end

"""
Create shared array
"""
#function getarray(y::Int, x::Int, z::Int)
function getarray(y::Int, x::Int, z::Int)::SharedArray{CalcType, 4}
    tmpfile = retrieve(conf, "calculation", "tmp_file_path")
    rm(tmpfile; force=true)

    SharedArray{CalcType}(tmpfile, (y, x, z, 3); mode="w+");
end

"""
Get array of circles centers with radius `r` represented by weighted to 1 coefficients
"""
function centers(img::Union{Array{T,2}, Array{T,3}}, r::Int)::Array{CalcType, 3} where T

    if length(size(img)) == 2
        # 2D case - single real image, no test mode
        cube, _ = hough_circles(img, r, r)
    else
        # 3D case - either array of real pictures or test cube with predefined circle centers
        y, x, _ = size(img)
        cube = zeros(CalcType, y, x, 1)

        istest = parse(Bool, retrieve(conf, "calculation", "istest"))
        if istest
            # we have test cube with already defined centers
            cube[:,:,1] = img[:,:,r]
        else
            # we have a set of real images for which we should squash accumulator values
            acc_threshold = parse(Int, retrieve(conf, "hough_circles", "acc_threshold"))

            for z in axes(img, 3)
                _, acc = hough_circles(img[:,:,z], r, r)

                for idx in findall(x -> x >= acc_threshold, acc)
                    cube[idx] += acc[idx]
                end
            end

            # we have single layer cube for specific radius value. we should normalize it
            # in order to make it comparable with centers weight coefficients calculated for other radiuses
            cmax = maximum(cube)
            if cmax > 0
                cube = cube / cmax
            end
        end
    end
    @info "radius: $r, circles found: $(length(cube[cube .> 0]))"

    cube
end

"""
Wave front start/end cut values for given circle center coordinate and corresponding input area side value.
Works for `X` and `Y` axes
"""
function _fse(c::Int, side::Int, fr::Int, flen::Int = 2fr + 1)
    if c <= fr
        s = fr + 1 - c + 1
        e = min(flen, side)
    elseif c > side - fr
        s = 1
        e = fr + 1 + (side - c)
    else
        s = 1
        e = min(flen, side)
    end

    s, e
end

"""
Cube start/end cut values for given wave fron center coordinate and corresponding input area side value.
Works for `X` and `Y` axes
"""
function _cse(c::Int, side::Int, fr::Int, flen::Int = 2fr + 1)
    if c <= fr + 1
        s = 1
        e = min(flen, side) - (fr - c) - 1
    elseif c > side - fr
        s = c - fr
        e = side
    else
        s = c - fr
        e = c + fr
    end

    s, e
end

"""
Wave front `2fr + 1` start/end range by `z` axis
we always start fron from the very bottom layer with maximum intensity
if center is more shallow than front radius than we have to cut wave front from the top accordingly
if used calculated **[rmin; rmax]** range is narrow than wave fron we have to cut wave front from top again
"""
function _fzse(r::Int, fr::Int, rmin::Int)
    # wave front is half sphere of "fr + 1" height. lower level has the higher intensity. same as Earth core to surface structure
    # we always cut wave front from top to preserve most intensive front values
    e = fr + 1

    if r <= fr
        s = e - r + 1
    else
        s = 1
    end

    # correction in case calc radiuses range is narrow than already cut wave front
    if e - s > r - rmin
        s = e - (r - rmin)
    end

    s, e
end

"""
Cube start/end range by `z` axis
we always start fron from the very bottom layer with maximum intensity
if center is more shallow than front radius than we have to cut wave front from the top accordingly
if used calculated **[rmin; rmax]** range is narrow than wave fron we have to cut wave front from top again
"""
function _czse(r::Int, fr::Int, rmin::Int)
    if r <= fr
        s = 1
    else
        # 1        50
        # 1                      95
        # 1-----------------90-------100
        s = r - fr
        s = max(s, rmin)
        s = s - rmin + 1
    end

    e = r - rmin + 1

    s, e
end

"""
Calculate edge parameters
"""
function edges(img::Union{Array{T,2}, Array{T,3}}, rmin::Int, rmax::Int) where T
    # 2d - real image; 3d - precalculated test cube or multilayer input image
    if length(size(img)) == 2
        ymax, xmax = size(img)
        zmax = rmax - rmin + 1
    else
        ymax, xmax, _ = size(img)
        zmax = rmax - rmin + 1

        istest = parse(Bool, retrieve(conf, "calculation", "istest"))
        if istest
            zmax = size(img, 3)
            rmin = max(rmin, 1)
            rmax = min(rmax, zmax)
        end
    end

    @info "Edges: ymax=$ymax, xmax=$xmax, zmax=$zmax, rmin=$rmin, rmax=$rmax"
    ymax, xmax, zmax, rmin, rmax
end

"""
Wave fronts interference calculation
By specifying different values for `rmin` and `rmax` it is possible to calculate any subset of radiuses.
For example `[20; 40]` will return cube of height 21 for calculated radiuses from the same range
"""
function waves3d(img::Union{Array{T,2}, Array{T,3}}, rmin::Int=1, rmax::Int=100, fr::Int=100)::Array{ParaViewType, 3} where T
    # get upper half of precalculated ideal spherical wave front of radius = fr
    # wave front with BetaPrime (weighted with coefficient 50) distribution decay on distance 100 gives amplitude decrease to 6%
    # size(front) = (201, 201, 101)
    front = genwave(fr)

    @info "Input area: $(size(img))"
    @info "Wave front: $(typeof(front)), size: $(round(sizeof(front)/1024/1024))"

    # calc edges and adjust `rmin` and `rmax` if needed
    ymax, xmax, zmax, rmin, rmax = edges(img, rmin, rmax)

    # accumulating waves impulses projections to coordinates (x, y, z)
    # will be populated for every point that might have non-zero value according to model
    proj = getarray(ymax, xmax, zmax)

    @info "Calc array: $(typeof(proj)), size: $(round(sizeof(proj)/1024/1024))"

    @info "Start processing layers"
    @time @sync @distributed for r in reverse(rmin:rmax)    # we couldn't use "r in reverse(1,z)" because we need actual "r" value
        # front start/end range by z axis
        fzs, fze = _fzse(r, fr, rmin)

        # proj start/end range by z axis
        pzs, pze = _czse(r, fr, rmin)

        # detect circle centers
        cube = centers(img, r)

        # inner loop by all circle centers
        @time for idx in findall(x -> x > 0, cube)
            yc = idx[1]
            xc = idx[2]

            # calc projection cube cut edges
            pys, pye = _cse(yc, ymax, fr)
            pxs, pxe = _cse(xc, xmax, fr)

            projv = @view proj[pys:pye, pxs:pxe, pzs:pze, :]

            # calc front cut edges
            fys, fye = _fse(yc, ymax, fr)
            fxs, fxe = _fse(xc, xmax, fr)

            # move wave front center to (yc, xc) and cut if necessary to fit into XY plane size
            wfront = @view front[fys:fye, fxs:fxe, fzs:fze]

            # here we must have 2 identical by sizes views for wave front and cube respectively
            # both views are generated for given (yc, xc, r) point
            #=
            @assert (size(wfront)[1] == size(projv)[1] &&
                    size(wfront)[2] == size(projv)[2] &&
                    size(wfront)[3] == size(projv)[3]) """wave front and cube views must be identical at first 3 dimensions.
                    wave: $(size(wfront)), cube: $(size(projv))
                    r: $r, fr: $fr, yc: $yc, xc: $xc,
                    pys: $pys, pye: $pye, pxs: $pxs, pxe: $pxe,
                    fys: $fys, fye: $fye, fxs: $fxs, fxe: $fxe
                    """
            =#

            # find coordinates of cutted wave front maximum. we know that there is only one maximum point with value 1
            wmax = maximum(wfront)
            wci = findall(x -> x == wmax, wfront)[1]

            # cycle by every non-zero point in cutted wave front
            for idx2 in findall(x -> x > 0, wfront)
                wdi = wci - idx2

                Δy = wdi[1]
                Δx = wdi[2]
                Δz = wdi[3]

                rlen = sqrt(Δx^2 + Δy^2 + Δz^2)

                # Projections approach
                if rlen != 0
                    projv[idx2, :] += cube[idx] * wfront[idx2] * [Δy, Δx, Δz] / rlen
                else
                    projv[idx2, :] += cube[idx] * wfront[idx2] * [0, 0, 1]
                end
                #@info "front: ($I), projv: $(projv[I])"

                # Angles approach gets identical but more quickly decayed by intensity result
                # there is also smaller intensity at the very center of wave, which is incorrect
                # Projections approach above has maximum intensity at the waves center
                #=
                if Δx == 0 && Δy == 0
                    projv[idx2, 1] += cube[idx] * wfront[idx2] / sqrt(3)  # x projection
                    projv[idx2, 2] += cube[idx] * wfront[idx2] / sqrt(3)  # y projection
                    projv[idx2, 3] += cube[idx] * wfront[idx2] / sqrt(3)  # z projection
                else
                    # intersection angle between radius vector projection on XY plane and X axis
                    α = atan(Δy/Δx)

                    # intersection angle between radius vector and XY plane
                    β = asin(Δz/rlen)
                    # intersection angle between radius vector and Z axis
                    θ = π/2 - β

                    # we don't sum vectors here. instead we project intensity in a given point onto the axes
                    # because "cos" is even function and "sin" is not we have to use absolute their values to act as a weight coefficients only
                    # impulse direction is dictated by radius vector
                    projv[idx2, 1] += cube[idx] * sign(Δx) * abs(cos(α)) * wfront[idx2]   # x projection
                    projv[idx2, 2] += cube[idx] * sign(Δy) * abs(sin(α)) * wfront[idx2]   # y projection
                    projv[idx2, 3] += cube[idx] * sign(Δz) * abs(cos(θ)) * wfront[idx2]   # z projection
                end
                =#
            end
        end

        GC.gc()
    end

    #proj = map(v -> sqrt(v[1]^2 + v[2]^2 + v[3]^2), proj[:,:,:])
    res = [sqrt(proj[i, j, k, 1]^2 + proj[i, j, k, 2]^2 + proj[i, j, k, 3]^2) for i in axes(proj, 1), j in axes(proj, 2), k in axes(proj, 3)];

    vmax = maximum(res)
    tmax = typemax(ParaViewType)

    if ParaViewType <: Integer
        round.(ParaViewType, res * tmax / vmax)
    else
        res
    end
end
