using CSV
using WriteVTK
using DataFrames
using Query

const prefix = "radius"
const ext = "csv"
const result_file = "cube_250_dispersion"

searchdir(path = ".") = filter(x -> startswith(x, prefix) & endswith(x, "." * ext), readdir(path))

getradius(filename) = parse(Int16, replace(replace(filename, prefix => ""), "." * ext => ""))

files = searchdir()
sort!(files, by = x -> getradius(x))

data = DataFrame(x = Int16[], y = Int16[], z = Int16[], value = Float32[])

for file in files
  println(file)
  #df = Int16.(CSV.read(file))
  df = CSV.read(file)
  df.x = Int16.(df.x)
  df.y = Int16.(df.y)
  df.z = Int16.(df.z)
  df.value = Float32.(df.value)

  global data

  data = vcat(data, df)

  GC.gc()
  GC.gc()
end

GC.gc()
GC.gc()

println(size(data))

Nx = maximum(data.x)
Ny = maximum(data.y)
Nz = maximum(data.z)

x = 1:1:Nx
y = 1:1:Ny
z = 1:1:Nz

val = zeros(Float32, Nx, Ny, Nz);

rows = @from i in data begin
  @select {i.x, i.y, i.z, i.value}
end;

for row in rows
  val[row.x, row.y, row.z] = row.value
end

#=
vtk = vtk_grid(result_file, x, y, z);
vtk_point_data(vtk, val, "density")
vtk_save(vtk)


julia> cell = val[1:Nx-1, 1:Ny-1, 1:Nz-1]
535×433×249 Array{Int8,3}:

julia> vtk_cell_data(vtk, cell, "cell_density")
VTK file 'cube_crop_cell_3x3.vti' (ImageData file, open)

julia> vtk_save(vtk)
1-element Array{String,1}:
 "cube_crop_cell_3x3.vti"
=#

vtk_write_array(result_file, val, "density")
