#!/usr/bin/env python
# coding: utf-8

import numpy as np
from pomegranate import *

mu1, sigma1 = 10, 4
mu2, sigma2 = 15, 2

def init_model(n = 1000):
    """Generate model as mix of two Gaussians with 1000 sample points"""

    np.random.seed(12341234)

    x = np.random.normal(mu1, sigma1, size=(n, 1))
    y = np.random.normal(mu2, sigma2, size=(n, 1))

    X = np.concatenate([x,y])

    return GeneralMixtureModel.from_samples(NormalDistribution, 2, X)

def gen_data(n = 5000000):
    """Generate n samples of data according to predefined Gaussians and tweak them a little bit"""

    x = np.random.normal(mu1, sigma1, size=(n, 1))
    y = np.random.normal(mu2, sigma2, size=(n, 1))

    # disturbe data
    x[::2]+=2
    x[::3]-=3
    y[::4]+=2

    X = np.concatenate([x, y])

    return X

def enable_GPU():
    """Check if GPU acceleration is enabled (requires cupy)"""

    print('Trying to enable GPU...')
    utils.enable_gpu()
    print('Check is GPU enabled', utils.is_gpu_enabled())

def run(elem, jobs = 1):
    """Run model initialization and fit"""

    model = init_model()
    data = gen_data()

    print('Start model', model)
    print('Data length', len(data), 'Jobs', jobs)

    model = model.fit(data, verbose = True, n_jobs = jobs)

    print('End model', model)
    print('Probability', elem, model.probability(elem))

if __name__ == "__main__":
    elem = [[12], [2], [7], [20]]
    enable_GPU()

    run(elem)
    print('--------------------------------------------------------------')
    run(elem, 4)
    print('Completed')
