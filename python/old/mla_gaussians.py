#!/usr/bin/env python
# coding: utf-8

"""Testing GMM algorithm"""

import numpy as np

from osgeo import gdal_array
from mla.gaussian_mixture import GaussianMixture


def init_model():
    """Generate model as mix of two Gaussians with 1000 sample points"""
    # gmm = GaussianMixture(K=5, init='kmeans', max_iters=500, tolerance=1e-3)
    gmm = GaussianMixture(K=5, max_iters=500, tolerance=1e-3)

    return gmm

def tif_data():
    """Load data from source *.tif file"""
    file = "../data/triangle.tif"

    # Read raster data as numeric array from file
    raster_array = gdal_array.LoadFile(file)

    #nodata = raster_array.max() # in here min value is nodata

    #Create a masked array for making calculations without nodata values
    #raster_array = np.ma.masked_equal(raster_array, nodata)

    # reshape raster_array
    size = raster_array.shape
    data = raster_array.reshape(size[0], size[1]*size[2]).transpose()
    data = data.astype(np.float64)

    print(data.shape, data.dtype)

    return data[0:10000]

def gen_data(n=1000):
    """Generate n samples of data according to predefined Gaussians and tweak them a little bit"""

    np.random.seed(12341234)

    mu1, sigma1 = 10, 4
    mu2, sigma2 = 15, 2

    x = np.random.normal(mu1, sigma1, size=(n, 1))
    y = np.random.normal(mu2, sigma2, size=(n, 1))

    # disturbe data
    x[::2] += 2
    x[::3] -= 3
    y[::4] += 2

    data = np.concatenate([x, y])

    return data

def run(elem):
    """Run model initialization and fit"""

    model = init_model()
    #data = gen_data()
    data = tif_data()

    print('Start model', model)
    print('Data length', len(data))

    model.fit(data)

    print('End model', model)

    elem = data[0:10]
    print('Probability', elem, model.predict(elem))


if __name__ == "__main__":
    #X = [[12], [2], [7], [20]]
    X = np.arange(7, 18, 2)

    run(X)
    print('Completed')
