#!/usr/bin/env python
# coding: utf-8

"""Store *.tif file data as *.csv file"""

import numpy as np
from sklearn import preprocessing as prep


from osgeo import gdal_array


def tif_data():
    """Load data from source *.tif file"""
    file = "../data/triangle.tif"

    # Read raster data as numeric array from file
    raster_array = gdal_array.LoadFile(file)

    #nodata = raster_array.max() # in here min value is nodata

    #Create a masked array for making calculations without nodata values
    #raster_array = np.ma.masked_equal(raster_array, nodata)

    # reshape raster_array
    size = raster_array.shape
    data = raster_array.reshape(size[0], size[1]*size[2]).transpose()
    #data = data.astype(np.float64)

    np.savetxt('../data/triangle.csv', data, fmt='%d', delimiter=',')

    # create the Scaler object
    scaler = prep.StandardScaler()

    # fit data on the scaler object
    scaled_data = scaler.fit_transform(data)

    np.savetxt('../data/triangle-normalized.csv', scaled_data, delimiter=',')

if __name__ == "__main__":
    tif_data()
