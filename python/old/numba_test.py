""" Testing numba library performance """
# import numpy as np
import time
import sys

from numba import jit, njit, jitclass, int32, int64

@njit
def calc_sum_standalone(n=1000):
    " Summarize 1 in a loop of n iterations"
    x = 0
    for _ in range(0, n):
        x += 1
        for _ in range(0, n):
            x += 1

    print('Counter', x)


@jitclass({'n': int64, 'calc_sum_standalone': (int32)})
class NumbaClass:
    """Testing Numba on class"""

    def __init__(self, n=1000):
        self.n = n

    def print_counter(self):
        """Printing loop size"""
        print('Size: ', self.n)

    def calc_sum_2(self):
        " Summarize 1 in a loop of n iterations"
        x = 0
        for _ in range(0, self.n):
            x += 1
            for _ in range(0, self.n):
                x += 1

        print('Counter', x)

    def calc_sum(self):
        " Summarize 1 in a loop of n iterations"
        calc_sum_standalone(self.n)


def main():
    """Running comparison test"""

    start_time = time.time()
    calc_sum_standalone(N)
    print('serial first run in %s seconds' % (time.time() - start_time))

    start_time = time.time()
    calc_sum_standalone(N)
    print('serial second run in %s seconds' % (time.time() - start_time))

    obj = NumbaClass(N)

    start_time = time.time()
    obj.calc_sum_2()
    print('numba first run in %s seconds' % (time.time() - start_time))

    start_time = time.time()
    obj.calc_sum_2()
    print('numba second run in %s seconds' % (time.time() - start_time))

    # start_time = time.time()
    # obj.numba_calc_sum(N)
    # print('numba third run in %s seconds' % (time.time() - start_time))

if __name__ == "__main__":
    if len(sys.argv) > 1:
        N = int(sys.argv[1])
    else:
        N = 1000

    main()
