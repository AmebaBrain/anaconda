#!/usr/bin/env python
# coding: utf-8

import numpy as np
import pomegranate as pg


def init_model(ndim = 10):
    """Generate model as mix of n Gaussians"""

    mu = np.arange(ndim)
    cov = np.eye(ndim)

    mgs = [ pg.MultivariateGaussianDistribution( mu*i, cov ) for i in range(ndim) ]
    gmm = pg.GeneralMixtureModel( mgs )

    return gmm


def gen_data(ndim = 10, rows = 1000000):
    """Generate n samples of data according to predefined Gaussians and tweak them a little bit"""

    np.random.seed(12341234)

    # generate data
    data = np.random.randn(rows, ndim) * ndim

    # disturbe data
    for i in range(ndim):
        data[i::ndim] += np.arange(ndim)*i

    return data


def enable_GPU():
    """Check if GPU acceleration is enabled (requires cupy)"""

    print('Trying to enable GPU...')
    pg.utils.enable_gpu()
    #pg.utils.disable_gpu()
    print('Check is GPU enabled', pg.utils.is_gpu_enabled())


def run(jobs = 1):
    """Run model initialization and fit"""

    model = init_model()
    data = gen_data()

    print('Start model', model)
    print('Data length', len(data), 'Jobs', jobs)

    model = model.fit(data, verbose=True, stop_threshold=1, n_jobs=jobs)

    print('End model', model)

    for i in range(10):
        print('Point {}: probability{}'.format( i, model.probability(data[i]) ))


if __name__ == "__main__":

    enable_GPU()

    #run()
    print('--------------------------------------------------------------')
    run(4)
    print('Completed')
