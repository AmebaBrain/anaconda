# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 22:19:34 2018

@author: Inquiring Mind
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import gdal
import os
from osgeo import gdal_array
import osr

#######################################

file = "triangle.tif"

# Read raster data as numeric array from file
rasterArray = gdal_array.LoadFile(file)
type(rasterArray) # is numpy.ndarray

rasterArray.min()
rasterArray.max() 
nodata = rasterArray.max() # in here min value is nodata

#Create a masked array for making calculations without nodata values
rasterArray = np.ma.masked_equal(rasterArray, nodata)
type(rasterArray)

# Check again array statistics
rasterArray.max()

# reshape rasterArray
size = rasterArray.shape
data = rasterArray.reshape(size[0], size[1]*size[2]).transpose()
data = data.astype(np.float32)
