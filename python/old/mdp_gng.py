"""
Testing Growing Neural Gas implementation by mdp package (Modular Toolkit for Data Processing)
"""

#import sys
import warnings

#with warnings.catch_warnings():
warnings.filterwarnings("ignore", category=DeprecationWarning)

def uniform(min_, max_, dims):
    """Return a random number between min_ and max_ ."""
    return mdp.numx_rand.random(dims)*(max_-min_)+min_

def circumference_distr(center, radius, n):
    """Return n random points uniformly distributed on a circumference."""
    phi = uniform(0, 2*mdp.numx.pi, (n, 1))
    x = radius*mdp.numx.cos(phi)+center[0]
    y = radius*mdp.numx.sin(phi)+center[1]
    return mdp.numx.concatenate((x, y), axis=1)

def circle_distr(center, radius, n):
    """Return n random points uniformly distributed on a circle."""
    n = int(n)
    phi = uniform(0, 2*mdp.numx.pi, (n, 1))
    sqrt_r = mdp.numx.sqrt(uniform(0, radius*radius, (n, 1)))
    x = sqrt_r*mdp.numx.cos(phi)+center[0]
    y = sqrt_r*mdp.numx.sin(phi)+center[1]
    return mdp.numx.concatenate((x, y), axis=1)

def rectangle_distr(center, w, h, n):
    """Return n random points uniformly distributed on a rectangle."""
    n = int(n)
    x = uniform(-w/2., w/2., (n, 1))+center[0]
    y = uniform(-h/2., h/2., (n, 1))+center[1]
    return mdp.numx.concatenate((x, y), axis=1)

def gen_data(n=2000):
    """ Generate data as a mix of 5 different geometrical figures """
    # Circumferences
    cf1 = circumference_distr([6, -0.5], 2, n)
    cf2 = circumference_distr([3, -2], 0.3, n)

    # Circles:
    cl1 = circle_distr([-5, 3], 0.5, n/2)
    cl2 = circle_distr([3.5, 2.5], 0.7, n)

    # Rectangles:
    rect1 = rectangle_distr([-1.5, 0], 1, 4, n)
    rect2 = rectangle_distr([+1.5, 0], 1, 4, n)
    rect3 = rectangle_distr([0, +1.5], 2, 1, n/2)
    rect4 = rectangle_distr([0, -1.5], 2, 1, n/2)

    # Shuffle the points to make the statistics stationary
    x = mdp.numx.concatenate([cf1, cf2, cl1, cl2, rect1, rect2, rect3, rect4], axis=0)
    x = mdp.numx.take(x, mdp.numx_rand.permutation(x.shape[0]), axis=0)

    return x


if __name__ == "__main__":

    import mdp
    mdp.numx_rand.seed(1266090063)

    X = gen_data()
