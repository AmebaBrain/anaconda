"""Visualization utilities"""

import random
import numpy as np
import matplotlib.pyplot as plt

from IPython import get_ipython


def init_pyplot():
    """Init pyplot library"""

    plt.style.use("ggplot")

    ipy = get_ipython()
    if ipy is not None:
        ipy.run_line_magic('matplotlib', 'osx')


def show_loss_acc(results):
    """Shows loss and accuracy graphs"""

    plt.figure(figsize=(8, 8))
    plt.title("Learning curve")
    plt.plot(results.history["loss"], label="loss")
    plt.plot(results.history["val_loss"], label="val_loss")
    plt.plot(np.argmin(results.history["val_loss"]), np.min(results.history["val_loss"]), \
        marker="x", color="r", label="best model")
    plt.xlabel("Epochs")
    plt.ylabel("log_loss")
    plt.legend()


def show_random(data, label):
    """Visualize any randome image along with the mask"""

    idx = random.randint(0, len(data))
    has_mask = label[idx].max() > 0 # salt indicator

    _fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 15))

    ax1.imshow(data[idx, ..., 0], cmap='seismic', interpolation='bilinear')
    if has_mask: # if salt
        # draw a boundary(contour) in the original image separating salt and non-salt areas
        ax1.contour(label[idx].squeeze(), colors='k', linewidths=5, levels=[0.5])
    ax1.set_title('Seismic')

    ax2.imshow(label[idx].squeeze(), cmap='gray', interpolation='bilinear')
    ax2.set_title('Salt')


def plot_sample(x, y, preds, binary_preds, idx=None):
    """Function to plot the results"""
    if idx is None:
        idx = random.randint(0, len(x))

    has_mask = y[idx].max() > 0

    _fig, axes = plt.subplots(1, 4, figsize=(20, 10))
    axes[0].imshow(x[idx, ..., 0], cmap='seismic')
    if has_mask:
        axes[0].contour(y[idx].squeeze(), colors='k', levels=[0.5])
    axes[0].set_title('Seismic')

    axes[1].imshow(y[idx].squeeze())
    axes[1].set_title('Salt')

    axes[2].imshow(preds[idx].squeeze(), vmin=0, vmax=1)
    if has_mask:
        axes[2].contour(y[idx].squeeze(), colors='k', levels=[0.5])
    axes[2].set_title('Salt Predicted')

    axes[3].imshow(binary_preds[idx].squeeze(), vmin=0, vmax=1)
    if has_mask:
        axes[3].contour(y[idx].squeeze(), colors='k', levels=[0.5])
    axes[3].set_title('Salt Predicted binary')
