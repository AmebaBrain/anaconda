"""Parse command line arguments"""

import argparse as ap

def cli(desc='U-net model implementation for semantic segmentation'):
    """ help reference and command line arguments processing """

    arg_parser = ap.ArgumentParser(description=desc, formatter_class=ap.RawTextHelpFormatter)
    arg_parser.add_argument('-c', required=False, default='unet.ini', \
        metavar=['config'], help='Config *.ini file to use')

    return arg_parser.parse_args(namespace=ap.Namespace())
