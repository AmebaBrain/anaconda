"""Prepare data from source images"""

import os
import numpy as np

from tqdm import tqdm_notebook
from keras.preprocessing.image import img_to_array, load_img
from skimage.transform import resize

def get_data(path, imh, imw):
    """Get and resize train images and masks"""

    ids = next(os.walk(path + "images"))[2]
    x = np.zeros((len(ids), imh, imw, 1), dtype=np.float32)
    y = np.zeros((len(ids), imh, imw, 1), dtype=np.float32)

    print('Getting and resizing images ... ')
    for n, id_ in tqdm_notebook(enumerate(ids), total=len(ids)):
        # Load images
        img = load_img(path + '/images/' + id_, grayscale=True)
        x_img = img_to_array(img)
        x_img = resize(x_img, (128, 128, 1), mode='constant', preserve_range=True)

        # Load masks
        mask = img_to_array(load_img(path + '/masks/' + id_, grayscale=True))
        mask = resize(mask, (128, 128, 1), mode='constant', preserve_range=True)

        # Save images
        x[n, ..., 0] = x_img.squeeze() / 255
        y[n] = mask / 255

    return x, y
