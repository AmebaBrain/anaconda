"""Main model building workflow"""

import configparser as cp
import numpy as np

from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.layers import Input
from keras.optimizers import Adam

from sklearn.model_selection import train_test_split

from unet.model import get_unet
from unet.data import get_data
from unet import graph

def main():
    """Create and train model"""

    graph.init_pyplot()

    config = cp.ConfigParser()
    config.read('unet.ini')

    # border = 5
    base_path = config['Data']['path']
    path_train = base_path + '/train'
    # path_test = '../input/test/'

    im_width = 128
    im_height = 128

    x, y = get_data(path_train, im_height, im_width)

    # Split train and validation data
    x_train, x_valid, y_train, y_valid = train_test_split(x, y, test_size=0.15, random_state=2018)

    graph.show_random(x_train, y_train)

    input_img = Input((im_height, im_width, 1), name='img')
    model = get_unet(input_img, n_filters=16, dropout=0.05, batchnorm=True)
    model.compile(optimizer=Adam(), loss="binary_crossentropy", metrics=["accuracy"])
    model.summary()

    model_name = config['Model']['name']

    callbacks = [
        EarlyStopping(patience=10, verbose=1),
        ReduceLROnPlateau(factor=0.1, patience=5, min_lr=0.00001, verbose=1),
        ModelCheckpoint(model_name, verbose=1, save_best_only=True, save_weights_only=True)
    ]

    results = model.fit(x_train, y_train, batch_size=32, epochs=50, callbacks=callbacks,\
                    validation_data=(x_valid, y_valid))

    graph.show_loss_acc(results)

    # load the best model
    model.load_weights(model_name)

    # evaluate on validation set (this must be equals to the best log_loss)
    model.evaluate(x_valid, y_valid, verbose=1)

    # predict on train, val and test
    preds_train = model.predict(x_train, verbose=1)
    preds_val = model.predict(x_valid, verbose=1)

    # threshold predictions
    preds_train_t = (preds_train > 0.5).astype(np.uint8)
    preds_val_t = (preds_val > 0.5).astype(np.uint8)


    # check if training data looks all right
    graph.plot_sample(x_train, y_train, preds_train, preds_train_t, idx=14)
    graph.plot_sample(x_train, y_train, preds_train, preds_train_t)
    graph.plot_sample(x_train, y_train, preds_train, preds_train_t)
    graph.plot_sample(x_train, y_train, preds_train, preds_train_t)
    graph.plot_sample(x_train, y_train, preds_train, preds_train_t)
    graph.plot_sample(x_train, y_train, preds_train, preds_train_t)
    graph.plot_sample(x_train, y_train, preds_train, preds_train_t)

    # check if valid data looks all right
    graph.plot_sample(x_valid, y_valid, preds_val, preds_val_t, idx=19)
    graph.plot_sample(x_valid, y_valid, preds_val, preds_val_t)
    graph.plot_sample(x_valid, y_valid, preds_val, preds_val_t)
    graph.plot_sample(x_valid, y_valid, preds_val, preds_val_t)
    graph.plot_sample(x_valid, y_valid, preds_val, preds_val_t)
    graph.plot_sample(x_valid, y_valid, preds_val, preds_val_t)
    graph.plot_sample(x_valid, y_valid, preds_val, preds_val_t)

    return model, results
