"""Model functions"""

from keras.models import Model
from keras.layers import BatchNormalization, Activation, Dropout
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate


def conv2d_block(input_tensor, n_filters, kernel_size=3, batchnorm=True):
    """Function to add 2 convolutional layers with the parameters passed to it"""

    # first layer
    x = Conv2D(filters=n_filters, kernel_size=(kernel_size, kernel_size), \
               kernel_initializer='he_normal', padding='same')(input_tensor)

    if batchnorm:
        x = BatchNormalization()(x)

    x = Activation('relu')(x)

    # second layer
    x = Conv2D(filters=n_filters, kernel_size=(kernel_size, kernel_size), \
               kernel_initializer='he_normal', padding='same')(input_tensor)

    if batchnorm:
        x = BatchNormalization()(x)

    x = Activation('relu')(x)

    return x

def get_unet(input_img, n_filters=16, dropout=0.1, batchnorm=True):
    """Function to define the U-net Model"""

    # Contracting Path
    c_1 = conv2d_block(input_img, n_filters * 1, kernel_size=3, batchnorm=batchnorm)
    p_1 = MaxPooling2D((2, 2))(c_1)
    p_1 = Dropout(dropout)(p_1)

    c_2 = conv2d_block(p_1, n_filters * 2, kernel_size=3, batchnorm=batchnorm)
    p_2 = MaxPooling2D((2, 2))(c_2)
    p_2 = Dropout(dropout)(p_2)

    c_3 = conv2d_block(p_2, n_filters * 4, kernel_size=3, batchnorm=batchnorm)
    p_3 = MaxPooling2D((2, 2))(c_3)
    p_3 = Dropout(dropout)(p_3)

    c_4 = conv2d_block(p_3, n_filters * 8, kernel_size=3, batchnorm=batchnorm)
    p_4 = MaxPooling2D((2, 2))(c_4)
    p_4 = Dropout(dropout)(p_4)

    c_5 = conv2d_block(p_4, n_filters=n_filters * 16, kernel_size=3, batchnorm=batchnorm)

    # Expansive Path
    u_6 = Conv2DTranspose(n_filters * 8, (3, 3), strides=(2, 2), padding='same')(c_5)
    u_6 = concatenate([u_6, c_4])
    u_6 = Dropout(dropout)(u_6)
    c_6 = conv2d_block(u_6, n_filters * 8, kernel_size=3, batchnorm=batchnorm)

    u_7 = Conv2DTranspose(n_filters * 4, (3, 3), strides=(2, 2), padding='same')(c_6)
    u_7 = concatenate([u_7, c_3])
    u_7 = Dropout(dropout)(u_7)
    c_7 = conv2d_block(u_7, n_filters * 4, kernel_size=3, batchnorm=batchnorm)

    u_8 = Conv2DTranspose(n_filters * 2, (3, 3), strides=(2, 2), padding='same')(c_7)
    u_8 = concatenate([u_8, c_2])
    u_8 = Dropout(dropout)(u_8)
    c_8 = conv2d_block(u_8, n_filters * 2, kernel_size=3, batchnorm=batchnorm)

    u_9 = Conv2DTranspose(n_filters * 1, (3, 3), strides=(2, 2), padding='same')(c_8)
    u_9 = concatenate([u_9, c_1])
    u_9 = Dropout(dropout)(u_9)
    c_9 = conv2d_block(u_9, n_filters * 1, kernel_size=3, batchnorm=batchnorm)

    outputs = Conv2D(1, (1, 1), activation='sigmoid')(c_9)
    model = Model(inputs=[input_img], outputs=[outputs])

    return model
