"""Running U-NET model on satellite image for the purpose of semantic segmentation"""

from unet.flow import main

MODEL, RESULTS = main()

if __name__ == "__main__":
    main()
