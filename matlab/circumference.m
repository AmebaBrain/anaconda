% Code to burn a white circle into a black digital image.
% Write white pixels along the circumference of the circle into an image.
% Create image of size 1200 rows by 2200 columns.
myImage = zeros(19, 19, 'uint8');
% Let's specify the center at (x, y) = (1600, 600) and the radius = 350.
xCenter = 10;
yCenter = 10;
radius = 9;
% Circumference for a circle of radius 350 should be 2*pi*r = 2199 pixels.
% To ensure that we have no gaps in the circle 
% we need to make sure we have at least as many coordinates in vectors x and y 
% as there are around the circumference of the circle.
% Make it double that just to make extra sure there are no gaps in the circle
% by going all 360 degrees (0 - 2*pi) with 4398 points.
theta = linspace(0, 2*pi, round(4 * pi * radius)); % Define angles
% Get x and y vectors for each point along the circumference.
x = radius * cos(theta) + xCenter;
y = radius * sin(theta) + yCenter;
plot(x, y);
axis square;
grid on;


% Write those (x,y) into the image with gray level 255.
for k = 1 : length(x)
    row = round(y(k));
    col = round(x(k));
    myImage(row, col) = 1;
end
% Display the image.  It may appear as though there are gaps in the circle
% due to subsampling for display but examine the image in the variable inspector
% and you'll see there are no gaps/breaks in the circle.
imshow(myImage);
axis('on', 'image');
% Plot crosshairs in the overlay at the center
hold on;
plot(xCenter, yCenter, 'r+', 'MarkerSize', 100);