limit = 250;
path = '../data/geo/';
rmin = 1;
rmax = limit;
step = 1;

% В переменную img загружается одноканальный геотиф, в georef помещается инфо о координатной привязке
[img, georef, ~] = geotiffread(strcat(path, 'lrr_4.tif')); 

% Обрезается геотиф и соответственно корректируется привязка для вырезанной области.
% Зачем - не помню, но это только для данного конкретного геотифа, можно просто игнорировать эти строки.
%A=double(img(500:end-500,500:end-500));
%georef(3,:) = georef(3,:)+500*georef(2,:)+500*georef(1,:);

% Часть комментов тут не мои (моего студента, что я когда-то подвязывал под эту тему)
%Перетворення Фурьє
F2=img; % копия входных данных
F1=fft2(F2); % Прямое двумерное Фурье-преобразование 

%Операція потрібна для обрахунку гаусівської кривизни
[col, row] = size(F2);

X=1:1:row;
Y=1:1:col;
[X1,Y1]=meshgrid(Y,X); % создается грид (регулярная 2Д-сетка на плоскости Х-У)


%Гаусівська кривизна запускаєте з іншого файлу.
%ans1 = gcurvature(X1,Y1,F1);
[ans1, H] = surfature(X1,Y1,F1); % sufrature - пользовательская функция, найденная в инете (тоже можешь найти и почитать).


%Бінаризація
FF = (ans1<0); % выбираются результаты с так называемой отрицательной гауссовской кривизной (которую расчитывает ф-я sufrature).
%imshow(FF); % визуализация

for i = 1:1:1

    % create an empty result file with header row
    file = strcat(path, 'cube', num2str(i), '.csv');
    fid = fopen(file, 'w');
    fprintf(fid, 'x,y,z,value\n');
    fclose(fid);


    % далее пошел цикл по радиусам окружностей (тут я задавал диск)
    % на каждом шаге цикла выполняется свертка диска с входным растром (FF); смотри чуть ниже wFF = filter2(w,FF);
    %ROUND
    for r = rmin:step:rmax % r - радиус маски
        clear w s
        w=fspecial('disk',r); % задание диска радиуса r
        s=mean2(w); % нахождение среднего значения

        % преобразование в логическую матрицу и нахождение суммы единиц -
        % определение суммарного числа единиц, находящихся на круге радиуса r
        clear sumw
        w(w>0 & w<=s)=1;
        w(w~=1)=0; % ~= в матлабе означает "не равно"
        sumw = sum(w(:)); % (:) - означает "взять все элементы"

        clear wFF SumFF
        wFF = filter2(w,FF);
        SumFF = wFF/sumw;
        %for i=r+1:1:row-r
        %    for j=r+1:1:col-r
        %        % поэлементное умножение маски (логического круга радиуса r) на
        %        % входные данные (FF - логический растр)
        %        wFF = w.*FF(i-r:i+r, j-r:j+r);
        %        SumFF(i,j) = sum(wFF(:))/sumw;
        %    end
        %end

        % далее необходимо найти те центры колец, которые удовлетворяют условию (SumFF >= 0.55)
        % тут S - результирующий растр такого же размера, как и входной
        % по умолчанию он инициализируется нулями
        % и затем позиции, удовлетворяющие условию, заменяются на 1
        clear S 
        S = zeros(size(SumFF)); % логический растр центров колец
        S(SumFF >= 0.55)=1; % критерий наличия центра кольца
        %S = logical(S);

        % adding convolution without dividing by total sum to get density value
        S = conv2(S, ones(3, 3), 'same');

        start_pos = 1 + limit*(i - 1);
        end_pos = limit*i;
        S = S(start_pos:end_pos, 1:limit);

        %clear row col radius val;  % do we need this? we unconditionally initialize them below
        [row, col, val] = find(S);
        col = col + (start_pos - 1);
        radius = ones(length(row), 1) * r;
        pos = [col row radius val];

        [row, col] = find(~S);
        col = col + (start_pos - 1);
        radius = ones(length(row), 1) * r;
        val = zeros(length(row), 1);
        neg = [col row radius val];

        res = [pos; neg];
        res = sortrows(res, [1 2]);

        %{
        file = strcat(path, 'radius', num2str(r), '.csv');
        fid = fopen(file, 'w');
        fprintf(fid, 'x,y,z,value\n');
        fclose(fid);
        %}

        dlmwrite(file, res, '-append');

        % cont(r) = sum(S(:)); % количество колец при данном r
    end

end

fprintf('completed\n')
