A = peaks(50)+randn(50); 
B = ones(3,3)/3^2; 
C = conv2(A,B,'same'); 
figure
subplot(1,2,1) 
imagesc(A)
axis image
colorbar
subplot(1,2,2) 
imagesc(C) 
axis image
colorbar