%r = 3;
w = fspecial('disk', r);
s = mean2(w);

w(w > 0 & w <= s) = 1;
w(w~=1) = 0;

r, size(w)
